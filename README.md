# Stream overlay

Stream overlay for casting QPL challengers and FragFest.

Preview: https://cdn.discordapp.com/attachments/728636157885153292/730790957020348496/overlay-final-test.mp4

# Run

-   download the latest [build:archive artifact](https://gitlab.com/robkorv/stream-overlay/-/pipelines?scope=branches&ref=master)
-   unzip stream-overlay.zip
-   start "server.exe" and keep it running.
-   to access the server from another device you have to allow "Public Networks" to access "server.exe" in your firewall.
    -   When you start "server.exe" for the first time, Windows Defender Firewall will ask which networks are allowed access to is. Tick "Public Networks" and click "Grand Access".
        ![windows-firewall.png](src/static/img/windows-firewall.png)
-   open http://overlay.localtest.me:5000/ and read the instructions.
-   you can navigate in the top to the different overlays, on the specific overlay page you will find instructions for OBS.
