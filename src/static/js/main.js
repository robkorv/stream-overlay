let previousData;

const logLine = (msg) => {
    let currentDate = new Date();
    return `[${currentDate.toISOString()}] ${msg}`;
};

let uRLSearchParams = new URLSearchParams(window.location.search);
let bodyClass = uRLSearchParams.get("body");
let bodyElement = document.querySelector("body");
if (!bodyClass) {
    bodyClass = "challengers";
}
bodyElement.className = bodyClass;

const updateOverlay = () => {
    let errorElement = document.querySelector(".error");
    fetch(`/static/config/${bodyClass}.json`, { cache: "no-store" })
        .then((response) => response.json())
        .then((data) => {
            for (let entry of errorElement.querySelectorAll("*")) {
                entry.remove();
            }
            let currentData = JSON.stringify(data);
            if (currentData !== previousData) {
                console.log(logLine("changes detected, updating!"));

                let info_1Element = document.querySelector(".info_1");
                info_1Element.innerText = data.info_1;

                let info_2Element = document.querySelector(".info_2");
                info_2Element.innerText = data.info_2;

                let blue_scoreElement = document.querySelector(".blue_score");
                blue_scoreElement.innerText = data.blue_score;
                let red_scoreElement = document.querySelector(".red_score");
                red_scoreElement.innerText = data.red_score;

                let blue_nameElement = document.querySelector(".blue_name");
                let red_playerElement = document.querySelector(".red_player");
                if (data.swap_names) {
                    blue_nameElement.innerText = data.red_player;
                    red_playerElement.innerText = data.blue_name;
                } else {
                    blue_nameElement.innerText = data.blue_name;
                    red_playerElement.innerText = data.red_player;
                }
            } else {
                console.log(logLine("no changes detected"));
            }
            previousData = currentData;
        })
        .catch((error) => {
            let msg = logLine(error);

            errorElement.insertAdjacentHTML(
                "beforeend",
                `<div class="relative">${msg}</div>`,
            );

            console.error(msg);
        });
};
updateOverlay();
setInterval(updateOverlay, 15 * 1000);
