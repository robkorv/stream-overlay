"""
Implementation of the GL & HF api
No documentation available
"""

import requests

BASE_URL = "https://glhf.zone/api/tournaments/participants/{tournament_id}/all_participants/?for_tourney_page=true"


def get_participants(tournament_id):
    return _do_request(tournament_id)


def _do_request(tournament_id):
    url = BASE_URL.format(tournament_id=tournament_id)
    headers = {
        "User-Agent": "gitlab.com/robkorv/stream-overlay",
        "Accept": "application/json",
    }
    with requests.get(url, headers=headers, timeout=30) as response:
        response.raise_for_status()
        result = response.json()
    return result
