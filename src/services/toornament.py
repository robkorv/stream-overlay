"""
Implementation of the Legacy Viewer API
https://developer.toornament.com/v2/doc/viewer_overview
"""

import requests

BASE_URL = "https://api.toornament.com/viewer/v2/tournaments/{tournament_id}/{endpoint}"


def get_matches(api_key, tournament_id):
    endpoint = "matches"
    max_range = 127
    range_ = f"{endpoint}=0-{max_range}"
    return _do_request(api_key, tournament_id, endpoint, range_)


def get_participants(api_key, tournament_id):
    endpoint = "participants"
    max_range = 49
    range_ = f"{endpoint}=0-{max_range}"
    return _do_request(api_key, tournament_id, endpoint, range_)


def _do_request(api_key, tournament_id, endpoint, range_):
    url = BASE_URL.format(tournament_id=tournament_id, endpoint=endpoint)
    headers = {
        "X-Api-Key": api_key,
        "Range": range_,
        "User-Agent": "gitlab.com/robkorv/stream-overlay",
    }
    with requests.get(url, headers=headers, timeout=30) as response:
        response.raise_for_status()
        result = response.json()
    return result
