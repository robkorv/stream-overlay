"""
Implementation of Challonge API v1
https://api.challonge.com/v1
"""
import requests

BASE_URL = "https://api.challonge.com/v1/tournaments/{tournament_id}/{endpoint}.json"


def get_participants(username, api_key, tournament_id):
    endpoint = "participants"
    return _do_request(username, api_key, tournament_id, endpoint)


def get_matches(username, api_key, tournament_id):
    endpoint = "matches"
    return _do_request(username, api_key, tournament_id, endpoint)


def _do_request(username, api_key, tournament_id, endpoint):
    url = BASE_URL.format(tournament_id=tournament_id, endpoint=endpoint)
    auth = (username, api_key)
    with requests.get(
        url,
        headers={"User-Agent": "gitlab.com/robkorv/stream-overlay"},
        auth=auth,
        timeout=30,
    ) as response:
        response.raise_for_status()
        result = response.json()
    return result
