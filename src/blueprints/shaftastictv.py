import datetime
import json
import pathlib
import re
import shutil
import socket
from urllib.parse import urlsplit

import flask
import requests
from services import challonge, glhf, toornament

ENCODING = "UTF-8"

RE_TEAM_BRACKETS = re.compile(r"(.+)\((.+)\)")
RE_GL_HF_TOURNEY_ID = re.compile(
    r"/api/tournaments/participants/(\d+)/all_participants\?for_tourney_page=true",
    re.IGNORECASE,
)

SOURCE_FILES = []
SOURCE_FILES.append(("left-name.txt", "left", "text"))
SOURCE_FILES.append(("left-player1.txt", "left player 1", "text"))
SOURCE_FILES.append(("left-player2.txt", "left player 2", "text"))
SOURCE_FILES.append(("left-score.txt", "0", "number"))
SOURCE_FILES.append(("right-name.txt", "right", "text"))
SOURCE_FILES.append(("right-player1.txt", "right player 1", "text"))
SOURCE_FILES.append(("right-player2.txt", "right player 2", "text"))
SOURCE_FILES.append(("right-score.txt", "0", "number"))
SOURCE_FILES.append(("score-combined.txt", "0:0", "text"))
SOURCE_FILES.append(("match-info.txt", "#.#", "text"))
SOURCE_FILES.append(("scrollingtext.txt", "    whoop whoop    \\o/", "text"))

MATCH_INFO_PREPENDS = [
    "WB",
    "1/4 Finals",
    "1/2 Finals",
    "WB Final",
    "LB Final",
    "LB",
    "LB Semi",
    "Grand Final",
]

MATCH_INFO_APPENDS = ["| BO1", "| BO3", "| BO5", "| BO7"]


bp = flask.Blueprint("shaftastictv", __name__, url_prefix="/shaftastictv")


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.10.10.10", 1))
        ip = s.getsockname()[0]
        return ip
    except Exception:
        raise
    finally:
        s.close()


def get_source_type(file_name):
    for name, _, source_type in SOURCE_FILES:
        if file_name == name:
            return source_type


def get_static_path():
    static_path = pathlib.Path(flask.current_app.static_folder).joinpath("shaftastictv")
    if not static_path.is_dir():
        static_path.mkdir()
    return static_path


def flash_message(message):
    # see https://github.com/pallets/flask/blob/0d8c8ba71bc6362e6ea9af08146dc97e1a0a8abc/src/flask/helpers.py#L400-L408
    flashes = flask.globals.request_ctx.flashes
    if flashes is None:
        flashes = flask.session.get("_flashes", [])
    flashes = [x[1] for x in flashes]
    if message not in flashes:
        flask.flash(message)


def delete_sesssion_key(key):
    if key in flask.session:
        del flask.session[key]


def init_text_files(txt_path):
    for file_name, default_text, _ in SOURCE_FILES:
        write_txt_file(txt_path, file_name, default_text)


def handle_txt_path(txt_path):
    result = False
    if txt_path and not pathlib.Path(txt_path).resolve().is_dir():
        message = f'txt path "{txt_path}" is not a directory'
        flash_message(message)
    elif txt_path and pathlib.Path(txt_path).resolve().is_dir():
        result = True

    return result


def get_settings():
    static_path = get_static_path()
    data_file = static_path.joinpath("settings.json")

    last_save = None
    data = {}
    if data_file.is_file():
        with open(data_file, encoding=ENCODING) as f:
            data = json.load(f)

        last_save = datetime.datetime.fromtimestamp(
            data_file.stat().st_mtime
        ).isoformat(sep=" ", timespec="seconds")
    return data, data_file, last_save


def write_txt_file(txt_path, file_name, content):
    if content:
        source_type = get_source_type(file_name)
        data, _, _ = get_settings()
        if source_type in ["text", "number"]:
            path = pathlib.Path(txt_path).joinpath(file_name).resolve()
            if path.is_file():
                with path.open("r", encoding=ENCODING) as f:
                    current_content = f.read()
            else:
                current_content = ""

            if content != current_content:
                with path.open("w", encoding=ENCODING) as f:
                    f.write(content)
                flash_message(f'{file_name} is now "{content}"')


def split_team_and_players(name):
    result = RE_TEAM_BRACKETS.search(name)

    if result:
        team = result.group(1).strip()
        players = sorted(
            (x.strip() for x in result.group(2).split(",")), key=lambda x: x.lower()
        )
    else:
        team = name
        players = []

    return team, players


@bp.before_request
def make_session_permanent():
    flask.session.permanent = True


@bp.route("/", methods=["POST", "GET"])
def index():
    try:
        ip = get_ip()
    except Exception as e:
        flask.current_app.logger.error(e)

    data, data_file, last_save = get_settings()

    if flask.request.method == "POST":
        form_data = flask.request.form.copy()

        challonge_tournament_url = form_data.get("challonge_tournament_url")
        challonge_tournament_id = data.get("challonge_tournament_id")
        if challonge_tournament_url and (
            challonge_tournament_url != data.get("challonge_tournament_url")
            or not challonge_tournament_id
        ):
            _, netloc, path, _, _ = urlsplit(challonge_tournament_url)
            challonge_tournament_id = ""
            paths = [x for x in path.split("/") if x.strip()]
            if paths:
                challonge_tournament_id = paths[0]
            netlocs = [x for x in netloc.split(".") if x.strip()]
            if (
                netlocs
                and challonge_tournament_id
                and netlocs[0] not in ["www", "challonge"]
            ):
                challonge_tournament_id = f"{netlocs[0]}-{challonge_tournament_id}"
            form_data["challonge_tournament_id"] = challonge_tournament_id
            flash_message(f'challonge_tournament_id set to "{challonge_tournament_id}"')
        elif challonge_tournament_url and challonge_tournament_id:
            form_data["challonge_tournament_id"] = challonge_tournament_id
        elif challonge_tournament_id:
            flash_message("challonge_tournament_id cleared")
            form_data["challonge_tournament_id"] = ""

        glhf_url = form_data.get("glhf_url")
        glhf_tournament_id = data.get("glhf_tournament_id")
        if glhf_url and (glhf_url != data.get("glhf_url") or not glhf_tournament_id):
            with requests.get(
                glhf_url,
                headers={"User-Agent": "gitlab.com/robkorv/stream-overlay"},
                timeout=30,
            ) as response:
                response.raise_for_status()
                re_match = RE_GL_HF_TOURNEY_ID.search(response.text)
                if re_match:
                    glhf_tournament_id = re_match.group(1)
                    form_data["glhf_tournament_id"] = glhf_tournament_id
                    flash_message(f'glhf_tournament_id set to "{glhf_tournament_id}"')
                else:
                    flash_message(
                        f"no glhf_tournament_id found in the content of {response.url}"
                    )
        elif glhf_url and glhf_tournament_id:
            form_data["glhf_tournament_id"] = glhf_tournament_id
        elif glhf_tournament_id:
            flash_message("glhf_tournament_id cleared")
            form_data["glhf_tournament_id"] = ""

        form_data["vs_mode_scrollingtext"] = data.get("vs_mode_scrollingtext")
        txt_path = form_data.get("txt_path")
        if handle_txt_path(txt_path):
            if txt_path != data.get("txt_path"):
                init_text_files(txt_path)
                form_data["vs_mode_scrollingtext"] = False

        with open(data_file, "w", encoding=ENCODING) as f:
            json.dump(form_data, f, indent=4)

        flash_message("saved!")
        return flask.redirect(flask.url_for("shaftastictv.index"))

    files_initiated = handle_txt_path(data.get("txt_path"))

    external_host = f"http://{ip}:5000/shaftastictv"
    return flask.render_template(
        "shaftastictv/index.html",
        data=data,
        last_save=last_save,
        files_initiated=files_initiated,
        external_host=external_host,
    )


@bp.route("/screen", methods=["POST", "GET"])
def screen():
    settings, settings_file, _ = get_settings()
    txt_path = settings.get("txt_path")
    static_path = get_static_path()

    flags = {}
    if settings.get("use_flags"):
        data_file = static_path.joinpath("flags.json")
        if data_file.is_file():
            with open(data_file, encoding=ENCODING) as f:
                flags = json.load(f)

    if not handle_txt_path(txt_path):
        flash_message("set a text path first")
        return flask.redirect(flask.url_for("shaftastictv.index"))

    if flask.request.method == "POST":
        if flask.request.form.get("reset") == "reset":
            init_text_files(txt_path)
            settings["vs_mode_scrollingtext"] = False
            settings["left_team"] = None
            settings["right_team"] = None
            with open(settings_file, "w", encoding=ENCODING) as f:
                json.dump(settings, f, indent=4)
            flash_message("reset done, GL&HF!")
            return flask.redirect(flask.url_for("shaftastictv.screen"))

        elif (
            flask.request.form.get("toornament_participants")
            == "toornament_participants"
        ):
            toornament_api_key = settings.get("toornament_api_key")
            toornament_tournament_id = settings.get("toornament_tournament_id")
            result = toornament.get_participants(
                toornament_api_key, toornament_tournament_id
            )

            for name in (x.get("name") for x in result):
                flash_message(f"toornament participant {name} added")

            data_file = static_path.joinpath("toornament_participants.json")
            with open(data_file, "w", encoding=ENCODING) as f:
                json.dump(result, f, indent=4)

            if settings.get("use_team_lineup"):
                data = {}
                for item in result:
                    team = item["name"]
                    players = [x["name"] for x in item["lineup"]]
                    data[item["id"]] = {"team": team, "players": players}

                data_file = static_path.joinpath("teams.json")
                with open(data_file, "w", encoding=ENCODING) as f:
                    json.dump(data, f, indent=4)

            return flask.redirect(flask.url_for("shaftastictv.screen"))

        elif (
            flask.request.form.get("challonge_participants") == "challonge_participants"
        ):
            challonge_username = settings.get("challonge_username")
            challonge_api_key = settings.get("challonge_api_key")
            challonge_tournament_id = settings.get("challonge_tournament_id")
            result = challonge.get_participants(
                challonge_username, challonge_api_key, challonge_tournament_id
            )

            for name in (x["participant"].get("name") for x in result):
                flash_message(f"challonge participant {name} added")

            data_file = static_path.joinpath("challonge_participants.json")
            with open(data_file, "w", encoding=ENCODING) as f:
                json.dump(result, f, indent=4)

            if settings.get("use_team_brackets_logic"):
                data = {}
                for item in result:
                    team, players = split_team_and_players(item["participant"]["name"])
                    data[item["participant"]["id"]] = {"team": team, "players": players}

                data_file = static_path.joinpath("teams.json")
                with open(data_file, "w", encoding=ENCODING) as f:
                    json.dump(data, f, indent=4)

            return flask.redirect(flask.url_for("shaftastictv.screen"))

        elif flask.request.form.get("glhf_participants") == "glhf_participants":
            glhf_tournament_id = settings.get("glhf_tournament_id")
            result = glhf.get_participants(glhf_tournament_id)

            for name in (x.get("custom_name") for x in result):
                flash_message(f"glhf participant {name} added")

            data_file = static_path.joinpath("glhf_participants.json")
            with open(data_file, "w", encoding=ENCODING) as f:
                json.dump(result, f, indent=4)

            return flask.redirect(flask.url_for("shaftastictv.screen"))

        left_name_txt = flask.request.form.get("left-name.txt", "").strip()
        left_player1_txt = flask.request.form.get("left-player1.txt", "").strip()
        left_player2_txt = flask.request.form.get("left-player2.txt", "").strip()
        left_score_txt = flask.request.form.get("left-score.txt", "0")
        right_name_txt = flask.request.form.get("right-name.txt", "").strip()
        right_player1_txt = flask.request.form.get("right-player1.txt", "").strip()
        right_player2_txt = flask.request.form.get("right-player2.txt", "").strip()
        right_score_txt = flask.request.form.get("right-score.txt", "0")
        match_info_txt = flask.request.form.get("match-info.txt", "").strip()
        prepend_match_info = flask.request.form.get("prepend_match_info", "").strip()
        append_match_info = flask.request.form.get("append_match_info", "").strip()
        scrollingtext_txt = (
            flask.request.form.get("scrollingtext.txt", "").replace("\\o/", "").strip()
        )
        vs_mode_scrollingtext = flask.request.form.get("vs_mode_scrollingtext", False)
        left_name = flask.request.form.get("left-name", "").strip()
        right_name = flask.request.form.get("right-name", "").strip()

        if flask.request.form.get("swap") == "swap":
            (
                left_name,
                left_name_txt,
                left_player1_txt,
                left_player2_txt,
                left_score_txt,
                right_name,
                right_name_txt,
                right_player1_txt,
                right_player2_txt,
                right_score_txt,
            ) = (
                right_name,
                right_name_txt,
                right_player1_txt,
                right_player2_txt,
                right_score_txt,
                left_name,
                left_name_txt,
                left_player1_txt,
                left_player2_txt,
                left_score_txt,
            )

        if settings.get("use_team_brackets_logic") or settings.get("use_team_lineup"):
            data_file = static_path.joinpath("teams.json")
            if data_file.is_file():
                with open(data_file, encoding=ENCODING) as f:
                    data = json.load(f)
                if left_name in data:
                    settings["left_team"] = left_name
                    left_name = data[left_name]["team"]
                if right_name in data:
                    settings["right_team"] = right_name
                    right_name = data[right_name]["team"]

                if settings.get("use_flags"):
                    flags_path = pathlib.Path(flask.current_app.static_folder).joinpath(
                        "img", "flags"
                    )
                    dest_path = pathlib.Path(txt_path)
                    if left_name in flags:
                        flag = flags[left_name]
                        flag_file = flags_path.joinpath(flag + ".png").resolve()
                        dest_file = dest_path.joinpath("left-flag.png").resolve()
                        if flag_file.is_file():
                            shutil.copyfile(flag_file, dest_file)
                    if right_name in flags:
                        flag = flags[right_name]
                        flag_file = flags_path.joinpath(flag + ".png").resolve()
                        dest_file = dest_path.joinpath("right-flag.png").resolve()
                        if flag_file.is_file():
                            shutil.copyfile(flag_file, dest_file)

        left_name_txt = left_name or left_name_txt
        right_name_txt = right_name or right_name_txt

        if prepend_match_info:
            current_prepend_filters = [
                (len(x), x) for x in MATCH_INFO_PREPENDS if match_info_txt.startswith(x)
            ]
            current_prepend_filter = (
                current_prepend_filters
                and max(current_prepend_filters, key=lambda x: x[0])[1]
            )
            if current_prepend_filter:
                match_info_txt = match_info_txt[len(current_prepend_filter) :].strip()
            match_info_txt = prepend_match_info + " " + match_info_txt

        if append_match_info:
            current_append_filters = [
                (len(x), x) for x in MATCH_INFO_APPENDS if match_info_txt.endswith(x)
            ]
            current_append_filter = (
                current_append_filters
                and max(current_append_filters, key=lambda x: x[0])[1]
            )
            if current_append_filter:
                match_info_txt = match_info_txt[: -len(current_append_filter)].strip()
            match_info_txt = match_info_txt + " " + append_match_info

        score_combined_txt = f"{left_score_txt}:{right_score_txt}"

        if vs_mode_scrollingtext:
            scrollingtext_txt = (
                f"{left_name_txt} [{score_combined_txt}] {right_name_txt}"
            )
        else:
            scrollingtext_txt = scrollingtext_txt.replace("v/s ", "")
            scrollingtext_txt = scrollingtext_txt.replace(
                f"[{score_combined_txt}] ", ""
            )
        scrollingtext_txt = f"    {scrollingtext_txt}    \\o/"

        settings_vs_mode_scrollingtext = settings.get("vs_mode_scrollingtext")
        if settings_vs_mode_scrollingtext != vs_mode_scrollingtext:
            settings["vs_mode_scrollingtext"] = vs_mode_scrollingtext
            flash_message(
                f"v/s mode for scrolling text is now {vs_mode_scrollingtext or 'off'}"
            )
        with open(settings_file, "w", encoding=ENCODING) as f:
            json.dump(settings, f, indent=4)

        write_txt_file(txt_path, "left-name.txt", left_name_txt)
        write_txt_file(txt_path, "left-player1.txt", left_player1_txt)
        write_txt_file(txt_path, "left-player2.txt", left_player2_txt)
        write_txt_file(txt_path, "left-score.txt", left_score_txt)
        write_txt_file(txt_path, "right-name.txt", right_name_txt)
        write_txt_file(txt_path, "right-player1.txt", right_player1_txt)
        write_txt_file(txt_path, "right-player2.txt", right_player2_txt)
        write_txt_file(txt_path, "right-score.txt", right_score_txt)
        write_txt_file(txt_path, "score-combined.txt", score_combined_txt)
        write_txt_file(txt_path, "match-info.txt", match_info_txt)
        write_txt_file(txt_path, "scrollingtext.txt", scrollingtext_txt)
        flash_message("saved!")
        return flask.redirect(flask.url_for("shaftastictv.screen"))

    file_details = {}
    last_save = None
    for name, _, source_type in SOURCE_FILES:
        path = pathlib.Path(txt_path).joinpath(name).resolve()
        if source_type in ["text", "number"]:
            with path.open(encoding=ENCODING) as f:
                content = f.read()
        else:
            content = ""
        if not last_save:
            last_save = datetime.datetime.fromtimestamp(path.stat().st_mtime).isoformat(
                sep=" ", timespec="seconds"
            )

        file_details.update({name: {"source_type": source_type, "content": content}})

    toornament_enabled = settings.get("integration") == "toornament"
    challonge_enabled = settings.get("integration") == "challonge"
    glhf_enabled = settings.get("integration") == "glhf"
    players = []
    teams = []
    players_left = []
    players_right = []
    if toornament_enabled:
        data_file = static_path.joinpath("toornament_participants.json")
        if data_file.is_file():
            with open(data_file, encoding=ENCODING) as f:
                data = json.load(f)
            players = [x.get("name", "").strip() for x in data]
            players.sort(key=lambda x: x.lower())
    elif challonge_enabled:
        data_file = static_path.joinpath("challonge_participants.json")
        if data_file.is_file():
            with open(data_file, encoding=ENCODING) as f:
                data = json.load(f)
            players = [x["participant"].get("name", "").strip() for x in data]
    elif glhf_enabled:
        data_file = static_path.joinpath("glhf_participants.json")
        if data_file.is_file():
            with open(data_file, encoding=ENCODING) as f:
                data = json.load(f)
            players = [x.get("custom_name", "").strip() for x in data]

    if settings.get("use_team_brackets_logic") or settings.get("use_team_lineup"):
        data_file = static_path.joinpath("teams.json")
        if data_file.is_file():
            with open(data_file, encoding=ENCODING) as f:
                data = json.load(f)
            teams = [(x, y["team"]) for x, y in data.items()]
            if left_team := data.get(settings.get("left_team")):
                players_left = left_team["players"]
            if right_team := data.get(settings.get("right_team")):
                players_right = right_team["players"]

    players.sort(key=lambda x: x.lower())
    teams.sort(key=lambda x: x[1].lower())

    return flask.render_template(
        "shaftastictv/screen.html",
        file_details=file_details,
        last_save=last_save,
        settings=settings,
        players=players,
        teams=teams,
        flags=flags,
        players_left=players_left,
        players_right=players_right,
        match_info_prepends=MATCH_INFO_PREPENDS,
        match_info_appends=MATCH_INFO_APPENDS,
    )
