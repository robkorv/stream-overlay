#!/usr/bin/env python3
import datetime
import io
import json
import pathlib
import socket
import sys

import flask
import qrcode
import qrcode.image.svg

LOCAL_HOST = "http://overlay.localtest.me:5000"


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.10.10.10", 1))
        ip = s.getsockname()[0]
        return ip
    except Exception:
        raise
    finally:
        s.close()


def create_app():
    app = flask.Flask(__name__)

    from blueprints import shaftastictv

    app.register_blueprint(shaftastictv.bp)

    CONFIG_PATH = pathlib.Path(app.static_folder).joinpath("config")
    CONFIGS = [x.name.replace(".json", "") for x in CONFIG_PATH.glob("*.json")]
    CONFIGS.sort()

    @app.route("/qr")
    def qr():
        data = flask.request.args.get("data", "")

        svg_image = qrcode.make(
            data,
            image_factory=qrcode.image.svg.SvgPathFillImage,
            box_size=30,
        )
        svg_stream = io.BytesIO()
        svg_image.save(svg_stream)
        svg_stream.seek(0)
        return flask.send_file(svg_stream, download_name="qr.svg")

    @app.route("/display")
    def display():
        return flask.render_template("display.html")

    @app.route("/<config>", methods=["POST", "GET"])
    def config(config):
        if config not in CONFIGS:
            flask.abort(404)

        data_file = CONFIG_PATH.joinpath(f"{config}.json")

        if flask.request.method == "POST":
            form_data = flask.request.form.copy()

            if "swap_names" in form_data:
                form_data["blue_name"] = flask.request.form["red_player"]
                form_data["red_player"] = flask.request.form["blue_name"]
            form_data["swap_names"] = 0

            form_data["blue_score"] = int(flask.request.form["blue_score"])
            form_data["red_score"] = int(flask.request.form["red_score"])

            with open(data_file, "w") as f:
                json.dump(form_data, f, indent=4)
            return flask.redirect(flask.url_for("config", config=config))

        try:
            ip = get_ip()
        except Exception as e:
            app.logger.error(e)

        external_host = f"http://{ip}:5000"
        qr_data = f"{external_host}/{config}"

        with open(data_file) as f:
            data = json.load(f)

        last_save = datetime.datetime.fromtimestamp(
            data_file.stat().st_mtime
        ).isoformat(sep=" ", timespec="seconds")

        return flask.render_template(
            "config.html",
            config=config,
            local_host=LOCAL_HOST,
            external_host=external_host,
            data=data,
            last_save=last_save,
            qr_data=qr_data,
        )

    @app.route("/")
    def index():
        try:
            ip = get_ip()
        except Exception as e:
            app.logger.error(e)

        external_host = f"http://{ip}:5000"

        return flask.render_template(
            "index.html", configs=CONFIGS, external_host=external_host
        )

    return app


if __name__ == "__main__":
    app = create_app()
    print(f"Serving from file://{app.root_path} at {LOCAL_HOST}")
    print("Press CTRL+C or close this terminal to exit the server.")
    if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
        print(
            "Windows Defender Firewall will ask which networks are allowed access."
            'Tick "Public Networks" and then click "Grand Access".'
        )
    app.config["SECRET_KEY"] = b"b\x18\x7fe\xe3\xd3\xe7\x18\xd9\x1b\xec\x84\x80\xd16}"
    try:
        app.run(host="0.0.0.0")
    except Exception as e:
        print(e)
        print("Something went wrong. Press a key to exit.")
        input()
        sys.exit(1)
